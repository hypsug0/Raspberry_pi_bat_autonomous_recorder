#!/bin/bash
##
##   -   '-'   -
##  / `.=) (=.` \
## /.-.-.\ /.-.-.\
##       'v'
##
## RaspPiBat recorder 2015
## Raspberry Pi Model A+/B+ with Cirrus Logic 192kHz SoundBoard // Version 20150727
## adapted from Son of 8-Bits
## http://sonof8bits.com/


## Stop the ntp service
sudo service ntp stop

## Stop the triggerhappy service
sudo service triggerhappy stop

## Stop the dbus service. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
sudo service dbus stop

## Stop the console-kit-daemon service. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
sudo killall console-kit-daemon

## Stop the polkitd service. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
sudo killall polkitd

## Kill the usespace gnome virtual filesystem daemon. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
killall gvfsd

## Kill the userspace D-Bus daemon. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
killall dbus-daemon

## Kill the userspace dbus-launch daemon. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
killall dbus-launch

## Set the CPU scaling governor to performance
# echo -n performance | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

exit
