# Read me
### A realtime bat autonomous recorder based on Raspberry Pi
---
***This project is now abandoned, replaced by the [PiBatRecorder](http://pibatrecorder.ardechelibre.org) project.***
---
This folder contains files needed to run the automated bat recorder, originally inspired from  "Raspberry Pi®Bat Project" : AK Fledermausschutz Aachen, Düren, Euskirchen (NABU/BUND/LNU)”

http://www.fledermausschutz.de/forschen/fledermausrufe-aufnehmen/raspberry-pi-bat-project/

https://www.raspberrypi.org/blog/bat-pi/

It is based on the platform Raspberry Pi (tested on [A+](https://www.raspberrypi.org/products/model-a-plus/) model ) with the [Cirrus Logic audio card](http://www.element14.com/community/community/raspberry-pi/raspberry-pi-accessories/cirrus_logic_audio_card) which can record up to 96kHz.

The ultrasonic microphone is home made, you can find those electronic circuits in "preamp" here. It consist in a FG Knowles electret ([FG-23629](https://www.digikey.com/product-detail/en/FG-23629-D65/423-1123-ND/810005) or [FG-23329](https://www.digikey.com/product-detail/en/FG-23329-D65/423-1119-ND/810837) mic with a low noise transistor preamp ([BC549c or BC550c](http://www.nxp.com/documents/data_sheet/BC549_550.pdf)).

![Alt text](https://framapic.org/5la9FkKDY6ul/48V8Lq96?dl). 

---
Recording scripts are in "rpibat" folder, placed at root.  They are launched by cron.
---
File & directory structure:

    |-- / [Root]
        |-- README.md
        |-- etc/
        |   |-- rc.local [script launched at boot, used to set time from the RTC Clock]
        |-- rpibat/
        |   |-- recordings.sh [start records with rec program (libSoX libs), launched by screenrecordings.sh]
        |   |-- screenrecordingsession.sh [use at start for background recording, launched by startrecording.sh]
        |   |-- startrecording.sh [script started by cron to initialize records, launched by cron at boot or at fixed time]
        |   |-- stoprecording.sh [script used to stop records by killing screen session]
        |   |-- stopservices.sh [script used to kill useless process]
        |-- var/
            |-- spool/
                |-- cron/
                    |-- crontabs/
                        |-- root [cron file used to program recordings]